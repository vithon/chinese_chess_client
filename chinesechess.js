/*

Copyright 2007 Nam T. Nguyen <bitsink@gmail.com>

*/

LOCAL_MODE = document.location.protocol == "file:";

SERVER_URL = "server.html"

MAX_ROW = 10;
MAX_COL = 9;

EMPTY = 0;
PAWN = 1;
CANNON = 2;
TANK = 3;
HORSE = 4;
ELEPHANT = 5;
GUARD = 6;
KING = 7;

RED_SIDE = 1;
BLACK_SIDE = 2;

UNIT_TYPE = ["empty", "pawn", "cannon", "tank", "horse", "elephant", "guard", "king"];


function encodeUri(params) {
	var ret = [];
	for (key in params) {
		ret.push(escape(key) + "=" + escape(params[key]));
	}
	return ret.join('&');
}


function generateGuid() {
	var result, i, j;
	result = '';
	for (j = 0; j < 32; j++)
	{
		if (j == 8 || j == 12 || j == 16 || j == 20) {
			result = result + '-';
		}
		i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
		result = result + i;
	}
	return result;
}


function generateBoardId() {
	return "board" + generateGuid();
}


function generatePieceId(boardId, row, col) {
	return boardId + "_" + row + "_" + col;
}


function getBoardId(pieceId) {
	return id.substring(0, pieceId.indexOf("_"));
}


function removeChildren(element) {
	while (element.childNodes.length) {
		element.removeChild(element.firstChild);
	}
}


function generateMoves(piece) {
	var generator = null;
	var generatorFunc = [EmptyMoveGenerator, PawnMoveGenerator, CannonMoveGenerator, TankMoveGenerator, HorseMoveGenerator, ElephantMoveGenerator, GuardMoveGenerator, KingMoveGenerator];
	if (piece.type >= 0 && piece.type < generatorFunc.length) {
		generator = new generatorFunc[piece.type](piece);
	} else {
		generator = new EmptyMoveGenerator(piece);
	}
	var nextLocs = generator.generateMoves();
	var moves = [];
	while (nextLocs.length > 0) {
		var loc = nextLocs.pop();
		if (loc[0] >= 0 && loc[0] < MAX_ROW && 
			loc[1] >= 0 && loc[1] < MAX_COL) {
			var p = piece.board.board[loc[0]][loc[1]];
			if (piece.canTakeOver(p)) {
				moves.push(loc);
			}
		}
	}
	return moves;
}


function EmptyMoveGenerator(piece) {
	this.generateMoves = function() {
		return [];
	}
}


function PawnMoveGenerator(piece) {
	this.board = piece.board.board;
	this.piece = piece;
	this.generateMoves = function() {
		function crossedRiver(piece) {
			if (piece.side == RED_SIDE) {
				return piece.row <= 4;
			} else {
				return piece.row >= 5;
			}
		}
		var nextLocs = [];
		var vr = -1;
		if (this.piece.side != RED_SIDE) {
			vr = 1;
		}
		nextLocs.push([this.piece.row + vr, this.piece.col]);
		if (crossedRiver(this.piece)) {
			nextLocs.push([this.piece.row, this.piece.col - 1]);
			nextLocs.push([this.piece.row, this.piece.col + 1]);
		}
		return nextLocs;
	};
}


function TankMoveGenerator(piece) {
	this.board = piece.board.board;
	this.piece = piece;
	this.velocity = [ [-1, 0], [1, 0], [0, -1], [0, 1] ];

	this.searchDirection = function(vr, vc) {
		var moves = [];
		var nr = this.piece.row;
		var nc = this.piece.col;
		while (true) {
			nr += vr;
			nc += vc;
			if (nr < 0 || nr >= MAX_ROW ||
				nc < 0 || nc >= MAX_COL) {
				break;
			}
			moves.push([nr, nc]);
			if (this.board[nr][nc].type != EMPTY) {
				if (this.board[nr][nc].side == this.piece.side) {
					moves.pop();
				}
				break;
			}
		}
		return moves;
	}

	this.generateMoves = function() {
		var nextLocs = [];
		for (var i = 0; i < this.velocity.length; i++) {
			var vr = this.velocity[i][0];
			var vc = this.velocity[i][1];
			var m = this.searchDirection(vr, vc);
			nextLocs = nextLocs.concat(m);
		}
		return nextLocs;
	}
}


function GuardMoveGenerator(piece) {

	this.piece = piece;
	this.board = piece.board.board;
	this.velocity = [[-1, -1], [-1, 1], [1, 1], [1, -1]];

	this.generateMoves = function() {
		function withinBound(piece, row, col) {
			var rowCheck = false;
			if (piece.side == RED_SIDE) {
				rowCheck = (row >= 7);
			} else {
				rowCheck = (row <= 2);
			}
			return rowCheck && (col >= 3 && col <= 5); 
		}
		var nextLocs = [];
		for (var i = 0; i < this.velocity.length; i++) {
			var vr = this.velocity[i][0];
			var vc = this.velocity[i][1];
			var nr = this.piece.row + vr;
			var nc = this.piece.col + vc;
			if (withinBound(this.piece, nr, nc)) {
				nextLocs.push([nr, nc]);
			}
		}
		return nextLocs;
	}

}


function KingMoveGenerator(piece) {

	this.piece = piece;
	this.board = piece.board.board;
	this.velocity = [[-1, 0], [1, 0], [0, 1], [0, -1]];

	this.generateMoves = function() {
		function withinBound(piece, row, col) {
			var rowCheck = false;
			if (piece.side == RED_SIDE) {
				rowCheck = (row >= 7);
			} else {
				rowCheck = (row <= 2);
			}
			return rowCheck && (col >= 3 && col <= 5); 
		}
		var nextLocs = [];
		for (var i = 0; i < this.velocity.length; i++) {
			var vr = this.velocity[i][0];
			var vc = this.velocity[i][1];
			var nr = this.piece.row + vr;
			var nc = this.piece.col + vc;
			if (withinBound(this.piece, nr, nc)) {
				nextLocs.push([nr, nc]);
			}
		}
		var vr = -1;
		if (this.piece.side != RED_SIDE) {
			vr = 1;
		}
		for (var i = piece.row + vr; MAX_ROW > i && i >= 0; i += vr) {
			if ((i == MAX_ROW || i == 0) && this.board[i][piece.col].type == KING) {
				nextLocs.push([i, piece.col]);
				break;
			}
			if (this.board[i][piece.col].type != EMPTY) {
				break;
			}
		}
		return nextLocs;
	}

}


function ElephantMoveGenerator(piece) {

	this.piece = piece;
	this.board = piece.board.board;
	this.velocity = [[-2, -2], [-2, 2], [2, 2], [2, -2]];
	this.block = [[-1, -1], [-1, 1], [1, 1], [1, -1]];

	this.generateMoves = function() {
		function withinBound(piece, row) {
			if (piece.side == RED_SIDE) {
				return row >= 5;
			} else {
				return row <= 4;
			}
		}
		var nextLocs = [];
		for (var i = 0; i < this.velocity.length; i++) {

			// calculate the blocking position
			var br = this.piece.row + this.block[i][0];
			var bc = this.piece.col + this.block[i][1];

			// blocking position is out of bound
			if (br < 0 || br >= MAX_ROW ||
				bc < 0 || bc >= MAX_COL) {
				continue;
			}

			// blocked, so skip it
			if (this.board[br][bc].type != EMPTY) {
				continue;
			}
			var vr = this.velocity[i][0];
			var vc = this.velocity[i][1];
			var nr = this.piece.row + vr;
			var nc = this.piece.col + vc;
			if (withinBound(this.piece, nr)) {
				nextLocs.push([nr, nc]);
			}
		}
		return nextLocs;
	}

}


function HorseMoveGenerator(piece) {

	this.piece = piece;
	this.board = piece.board.board;
	this.velocity = [[-2, -1], [-2, 1], [-1, 2], [1, 2], [2, 1], [2, -1], [1, -2], [-1, -2]];
	this.block = [[-1, 0], [-1, 0], [0, 1], [0, 1], [1, 0], [1, 0], [0, -1], [0, -1]];

	this.generateMoves = function() {
		var nextLocs = [];
		for (var i = 0; i < this.velocity.length; i++) {
			var br = this.piece.row + this.block[i][0];
			var bc = this.piece.col + this.block[i][1];
			if (br < 0 || br >= MAX_ROW ||
				bc < 0 || bc >= MAX_COL) {
				continue;
			}
			if (this.board[br][bc].type != EMPTY) {
				continue;
			}
			var vr = this.velocity[i][0];
			var vc = this.velocity[i][1];
			var nr = this.piece.row + vr;
			var nc = this.piece.col + vc;
			nextLocs.push([nr, nc]);
		}
		return nextLocs;
	}

}


function CannonMoveGenerator(piece) {
	this.board = piece.board.board;
	this.piece = piece;
	this.velocity = [ [-1, 0], [1, 0], [0, -1], [0, 1] ];

	this.searchDirection = function(vr, vc) {
		var moves = [];
		var nr = this.piece.row;
		var nc = this.piece.col;
		while (true) {
			nr += vr;
			nc += vc;
			// out of bound
			if (nr < 0 || nr >= MAX_ROW ||
				nc < 0 || nc >= MAX_COL) {
				break;
			}
			if (this.board[nr][nc].type != EMPTY) {
				// looking ahead
				while (true) {
					nr += vr;
					nc += vc;
					// out of bound
					if (nr < 0 || nr >= MAX_ROW ||
						nc < 0 || nc >= MAX_COL) {
						break;
					}
					// empty square, continue looking
					if (this.board[nr][nc].type == EMPTY) {
						continue;
					}
					// found an opponent
					if (this.board[nr][nc].side != this.piece.side) {
						moves.push([nr, nc]);
					}
					break;
				}
				break;
			} else {
				moves.push([nr, nc]);
			}
		}
		return moves;
	}

	this.generateMoves = function() {
		var nextLocs = [];
		for (var i = 0; i < this.velocity.length; i++) {
			var vr = this.velocity[i][0];
			var vc = this.velocity[i][1];
			var m = this.searchDirection(vr, vc);
			nextLocs = nextLocs.concat(m);
		}
		return nextLocs;
	}
}


function Piece(board, row, col, type, side) {
	this.board = board;
	this.row = row;
	this.col = col;
	this.side = side;
	this.type = type;
	this.name = "empty";
	this.nextMoves = [];

	if (this.type < UNIT_TYPE.length) {
		this.name = UNIT_TYPE[this.type];
	}

	/* this.print = function() {
		if (this.side == RED_SIDE) {
			document.write("<font color=\"red\">");
		} else {
			document.write("<font color=\"black\">");
		}
		document.write(this.name);
		document.write("</font>");
	} */

	this.canTakeOver = function(piece) {
		return (piece.type == EMPTY) || 
			(this.side != piece.side);
	};

	this.generateNextMoves = function() {
		this.nextMoves = generateMoves(this);
	}
}


function Board() {
	this.board = new Array(MAX_ROW);
	for (var i = 0; i < MAX_ROW; i++) {
		this.board[i] = new Array(MAX_COL);
	}

	this.initialize = function(config) {
		var board = config.board;
		for (var row = 0; row < MAX_ROW; row++) {
			for (var col = 0; col < MAX_COL; col++) {
				var type = board[row][col][0];
				var side = board[row][col][1];
				this.board[row][col] = new Piece(this, row, col, type, side);
			}
		}
	};

	this.movePiece = function(from, to) {
		if (from.type == EMPTY) {
			return;
		}
		var tmp = from;
		this.board[from.row][from.col] = new Piece(this, from.row, from.col, EMPTY, 0);
		this.board[to.row][to.col] = new Piece(this, to.row, to.col, tmp.type, tmp.side);
	};

}


function ChineseChessView(boardId) {

	this.boardId = boardId
	this.container = document.createElement("div");
	this.container.className = "board";
	this.container.id = this.boardId;

	this.initialize = function(config) {
		for (var row = 0; row < MAX_ROW; row++) {
			var rowClass = "boardRow boardRow" + row;
			for (var col = 0; col < MAX_COL; col++) {
				var columnClass = rowClass + " boardColumn boardColumn" + col;
				var cellClass = columnClass + " boardCell boardCell" + row + col; 
				ee = document.createElement("div");
				ee.className = cellClass;
				var pe = document.createElement("div");
				pe.id = generatePieceId(this.boardId, row, col);
				ee.appendChild(pe);
				this.container.appendChild(ee);
			}
		}
		var redStatus = document.createElement("div");
		redStatus.className = "redStatus";
		redStatus.id = this.boardId + "_red";
		this.container.appendChild(redStatus);
		var blackStatus = document.createElement("div");
		blackStatus.className = "blackStatus";
		blackStatus.id = this.boardId + "_black";
		this.container.appendChild(blackStatus);
	}

	this.generateClassName = function(piece) {
		var className = "";
		if (piece.type != EMPTY) {
			if (piece.side == RED_SIDE) {
				className = "red_";
			} else {
				className = "black_";
			}
		}
		className += piece.name;
		return className;
	};

	this.update = function(board, turnSide, winSide) {
		for (var row = 0; row < MAX_ROW; row++) {
			for (var col = 0; col < MAX_COL; col++) {
				var cellId = generatePieceId(this.boardId, row, col);
				var div = document.getElementById(cellId);
				div.piece = board[row][col];
				div.className = this.generateClassName(board[row][col]);
			}
		}
		removeChildren(document.getElementById(this.boardId + "_red"));
		removeChildren(document.getElementById(this.boardId + "_black"));
		var turn = document.createElement('div');
		turn.className = "turn";
		var div = null;
		if (turnSide == RED_SIDE) {
			div = document.getElementById(this.boardId + "_red");
		} else {
			div = document.getElementById(this.boardId + "_black");
		}
		div.appendChild(turn);
		if (!winSide) {
			return;
		}
		if ((winSide & RED_SIDE) != 0) {
			var win = document.createElement('div');
			win.className = 'winner';
			div = document.getElementById(this.boardId + "_red");
			div.appendChild(win);
		}
		if ((winSide & BLACK_SIDE) != 0) {
			var win = document.createElement('div');
			win.className = 'winner';
			div = document.getElementById(this.boardId + "_black");
			div.appendChild(win);
			
		}
	};

	this.mark = function(locs) {
		for (var i = 0; i < locs.length; i++) {
			var loc = locs[i];
			var cellId = generatePieceId(this.boardId, loc[0], loc[1]);
			var div = document.getElementById(cellId);
			div.style.backgroundColor = "blue";
			div.style.opacity = "0.4";
			div.style.cursor = "pointer";
		}
	}

	this.demark = function(locs) {
		for (var i = 0; i < locs.length; i++) {
			var loc = locs[i];
			var cellId = generatePieceId(this.boardId, loc[0], loc[1]);
			var div = document.getElementById(cellId);
			div.style.backgroundColor = "";
			div.style.opacity = "";
			div.style.cursor = "";
		}
	}

	/* this.print = function() {
		for (row = 0; row < MAX_ROW; row++) {
			for (col = 0; col < MAX_COL; col++) {
				var p = this.board[row][col];
				p.print();
				document.write(" ");
			}
			document.write("<br />");
		}
	}; */

}


function MatchConfiguration(element, side) {
	
	this.visitBoardConfiguration = function(element) {
		var lookupTable = {'t': TANK, 'h': HORSE, 'e': ELEPHANT,
			'g': GUARD, 'k': KING, 'c': CANNON, 'p': PAWN};
		var lines = element.getElementsByTagName("div");
		this.board = new Array(MAX_ROW);
		for (var row = 0; row < MAX_ROW; row++) {
			var line = lines[row].firstChild.nodeValue;
			this.board[row] = new Array(MAX_COL);
			for (var col = 0; col < MAX_COL; col++) {
				var c = line.charAt(col);
				var side = RED_SIDE;
				var type = EMPTY;
				if ('A' <= c && c <= 'Z') {
					side = BLACK_SIDE;
					c = c.toLowerCase();
				}
				if (c in lookupTable) {
					type = lookupTable[c];
				}
				this.board[row][col] = [type, side];
			}
		}
	}

	var divs = element.getElementsByTagName("div");
	for (var i = 0; i < divs.length; i++) {
		var div = divs[i];
		if (div.className == "boardConfiguration") {
			this.visitBoardConfiguration(div);
		}
	}

}


function HistoryView() {
	this.container = document.createElement("div");
	this.container.className = "history";
	this.content = document.createElement("div");
	this.content.className = "historyContent";
	this.control = document.createElement("div");
	this.control.className = "historyControl";
	this.container.appendChild(this.control);
	this.container.appendChild(this.content);
	this.backButton = document.createElement("span");
	this.backButton.className = "historyBack";
	this.backButton.innerHTML = "&lt;---";
	this.forwardButton = document.createElement("span");
	this.forwardButton.className = "historyForward";
	this.forwardButton.innerHTML = "---&gt;";
	this.control.appendChild(this.backButton);
	this.control.appendChild(this.forwardButton);

	this.update = function(moves) {
		var colNames = ["a", "b", "c", "d", "e", "f", "g", "h", "i"];
		removeChildren(this.content);
		for (var i = moves.length; i > 0; i--) {
			var move = moves[i - 1];
			var li = document.createElement("div");
			li.innerHTML = i + "&mdash;" + colNames[move[1]] + (MAX_ROW - move[0]) + " " + colNames[move[3]] + (MAX_ROW - move[2]);
			this.content.appendChild(li);
		}
	}
}


function ChineseChessController(element, side) {

	this.board = new Board();
	this.config = new MatchConfiguration(element, side);
	this.boardId = generateBoardId();
	this.view = new ChineseChessView(this.boardId);
	this.historyView = new HistoryView();
	this.element = element;
	// red always go first
	this.nextPlayer = RED_SIDE;
	this.selectedPiece = null;
	this.moves = [];
	this.lastUpdateTime = 0;
	this.updateTimer = null;
	this.serverUrl = null;
	this.historyLevel = 0;
	this.side = side;
	this.winner = 0;

	this.start = function(server_url) {
		this.serverUrl = server_url;
		this.view.initialize(this.config);
		this.board.initialize(this.config);
		removeChildren(this.element);
		this.element.appendChild(this.historyView.container);
		this.element.appendChild(this.view.container);
		this.view.update(this.board.board, this.nextPlayer, 0);
		this.installHistoryListener();
		this.installMouseListener();
		this.installRefreshListener();
	}

	this.installHistoryListener = function() {
		this.historyView.backButton.controller = this;
		this.historyView.backButton.onclick = this.handleHistoryBack;
		this.historyView.forwardButton.controller = this;
		this.historyView.forwardButton.onclick = this.handleHistoryForward;
	}

	this.buildBoard = function(historyLevel) {
		var board = new Board();
		board.initialize(me.config);
		for (var i = 0; i < historyLevel; i++) {
			var row = me.moves[i][0];
			var col = me.moves[i][1];
			var from = board.board[row][col];
			row = me.moves[i][2];
			col = me.moves[i][3];
			var to = board.board[row][col];
			board.movePiece(from, to);
		}
		return board;
	}

	this.handleHistoryBack = function() {
		me = this.controller;
		if (me.historyLevel <= 0) {
			return;
		}
		me.historyLevel--;
		var board = me.buildBoard(me.historyLevel);
		me.view.update(board.board, me.nextPlayer, me.winner);
	}

	this.handleHistoryForward = function() {
		me = this.controller;
		if (me.historyLevel >= me.moves.length) {
			return;
		}
		me.historyLevel++;
		var board = me.buildBoard(me.historyLevel);
		me.view.update(board.board, me.nextPlayer, me.winner);
	}

	this.sendUpdate = function() {
		var ajax = new XHR( {method: "post",
			onSuccess: this.sendUpdateSuccess,
			onFailure: this.sendUpdateFailure,
			isSuccess: function(status) {
				return status == 0 || (status >= 200 && status < 300);
			}
		} );
		ajax.controller = this;
		if (!LOCAL_MODE) {
			ajax.send(this.serverUrl, encodeUri( { "action": "update",
				"index": this.moves.length} ));
		} else {
			ajax.send(this.serverUrl + this.moves.length);
		}
	}

	this.sendUpdateSuccess = function(text, xml) {
		controller = this.controller;
		var lines = text.split(/\r?\n/);
		var index = new Number(lines.shift()).valueOf();
		for (var i = index; i < controller.moves.length; i++) {
			lines.shift();
		}
		for (var i = 0; i < lines.length; i++) {
			var line = lines[i];
			if ('M' == line.charAt(0)) {
				var move = [];
				for (var j = 0; j < 4; j++) {
					move[j] = (new Number(line.charAt(j + 1))).valueOf();
				}
				// controller.nextPlayer =  (new Number(line.charAt(5))).valueOf();
				controller.executeMove(move);
			} else if ('E' == line.charAt(0)) {
				controller.winner = (new Number(line.charAt(1))).valueOf();
				controller.gameOver(line);
			}
		}
		// window.focus();
	}

	this.gameOver = function(line) {
		this.side = 0;
		this.view.update(this.board.board, this.nextPlayer, this.winner);
		$clear(this.updateTimer);
	}

	this.sendUpdateFailure = function() {
		// ignored
	}

	this.handleTimer = function(interval) {
		var now = (new Date()).getTime();
		if (now - this.lastUpdateTime < interval) {
			return;
		}
		this.sendUpdate();
		this.lastUpdateTime = (new Date()).getTime();
	}

	this.installRefreshListener = function() {
		this.lastUpdateTime = (new Date()).getTime();
		this.updateTimer = this.handleTimer.periodical(5000, this, 5000);
	}

	this.installMouseListener = function() {
		for (var row = 0; row < MAX_ROW; row++) {
			for (var col = 0; col < MAX_COL; col++) {
				var cellId = generatePieceId(this.boardId, row, col);
				var div = document.getElementById(cellId);
				div.onclick = this.handleClick;
				div.controller = this;
			}
		}
	}
	
	this.sendMove = function(index, move) {
		var ajax = new XHR( {method: "post",
			onSuccess: this.sendMoveSuccess,
			onFailure: this.sendMoveFailure,
			isSuccess: function(status) {
				return status == 0 || (status >= 200 && status < 300);
			}
		} );
		ajax.controller = this;
		if (!LOCAL_MODE) {
			ajax.send(this.serverUrl, encodeUri( {"action": "move",
				"row1": move[0],
				"col1": move[1],
				"row2": move[2],
				"col2": move[3] } ));
		} else {
			ajax.send(this.serverUrl + index);
		}
	}

	this.executeMove = function(pendingMove) {
		try {
			soundManager.play(this.boardId + "_sound", '/sound/wood.mp3');
		} catch (err) {
			// ignored alert(err);
		}
		this.moves.push(pendingMove);
		var row = pendingMove[0];
		var col = pendingMove[1];
		var from = this.board.board[row][col];
		row = pendingMove[2];
		col = pendingMove[3];
		var to = this.board.board[row][col];
		this.board.movePiece(from, to);
		if (this.nextPlayer == RED_SIDE) {
			this.nextPlayer = BLACK_SIDE;
		} else {
			this.nextPlayer = RED_SIDE;
		}
		this.view.update(this.board.board, this.nextPlayer, this.winner);
		this.historyView.update(this.moves);
		this.historyLevel = this.moves.length;
	}

	this.sendMoveSuccess = function(text, xml) {
		var controller = this.controller;
		controller.executeMove(controller.pendingMove);
	}

	this.sendMoveFailure = function(transport) {
		// ignored
	}

	this.handleSecondClick = function(piece) {
		if (this.nextPlayer != this.side || this.historyLevel != this.moves.length) {
			return;
		}
		var nextMoves = this.selectedPiece.nextMoves;
		for (var i = 0; i < nextMoves.length; i++) {
			var row = nextMoves[i][0];
			var col = nextMoves[i][1];
			if (piece.row == row && piece.col == col) {
				this.pendingMove = [this.selectedPiece.row, this.selectedPiece.col, piece.row, piece.col];
				this.sendMove(this.moves.length, this.pendingMove);
			}
		}
		this.view.demark(nextMoves);
		this.selectedPiece = null;
	}

	this.handleFirstClick = function(piece) {
		if (this.nextPlayer != this.side || this.historyLevel != this.moves.length) {
			return;
		}
		piece.generateNextMoves();
		if (piece.nextMoves.length == 0) {
			return;
		}
		this.selectedPiece = piece;
		this.view.mark(piece.nextMoves);
	}

	this.handleClick = function(evt) {
		var piece = this.piece;
		var controller = this.controller;
		if (controller.selectedPiece != null) {
			controller.handleSecondClick(piece);
		} else if (piece.type == EMPTY || controller.side != piece.side) {
			return;
		} else {
			controller.handleFirstClick(piece);
		}
	}

}


function initBoard(side, server_url) {
	var elements = Array();
	function initElement(element) {
		if ("chineseChess" == element.className) {
			elements.push(element);
		} else if (element.hasChildNodes()) {
			for (var i = 0; i < element.childNodes.length; i++) {
				initElement(element.childNodes[i]);
			}
		}
	}
	initElement(document.documentElement);
	while (elements.length) {
		var element = elements.pop();
		var controller = new ChineseChessController(element, side);
		if (server_url == null) {
			server_url = SERVER_URL;
		}
		controller.start(server_url);
	}
}


function sendDraw(server_url) {
	var ajax = new XHR( {method: "post",
		onSuccess: function() { /* ignored */ },
		onFailure: function() { /* ignored */ },
		isSuccess: function(status) {
			return status == 0 || (status >= 200 && status < 300);
		}
	} );
	ajax.send(server_url, encodeUri({'action': 'draw'}));
}


function sendResign(server_url) {
	var ajax = new XHR( {method: "post",
		onSuccess: function() { /* ignored */ },
		onFailure: function() { /* ignored */ },
		isSuccess: function(status) {
			return status == 0 || (status >= 200 && status < 300);
		}
	} );
	ajax.send(server_url, encodeUri({'action': 'resign'}));
}
