//---------------------------------------------------------------------------
Ajax = function() {
	return this;
}
//---------------------------------------------------------------------------
Ajax.timeout = function() {	// over 10 seconds (too long ?!)
	return ((new Date()).getTime() - Ajax.time) / 1000 > 10;
}
//---------------------------------------------------------------------------
Ajax.prepare = function(method, url, params, callback, onerror, ontimeout) {
	var r = null;
	if (window.XMLHttpRequest) { // native XMLHttpRequest object
		r = new XMLHttpRequest();
	} else if (window.ActiveXObject) { // IE/Windows ActiveX version
		r = new ActiveXObject('Microsoft.XMLHTTP');
	}

	if (r != null) {
		r.url = ''+url;
		r.method = ''+method;
		r.params = ''+params;
		r.callback = callback;
		r.onreadystatechange = Ajax.recv;
		r.onerror = onerror;
		r.ontimeout = ontimeout;
	}

	return r;
}
//---------------------------------------------------------------------------
Ajax.execute = function(request) {
	var r = request;

	if (r == null || r == undefined) {
		Ajax.request = null;
		Ajax.time = 0;
		return;
	}
	Ajax.time = (new Date()).getTime();
	if (Ajax.request)
		Ajax.request.onreadystatechange = null;
	Ajax.request = request;

	r.open(r.method, r.url, true);
	if (r.method == 'POST') {
		r.setRequestHeader('Content-Type',
			'application/x-www-form-urlencoded; charset=UTF-8');
	}

	r.send(r.params);
}
//---------------------------------------------------------------------------
Ajax.test = function(url, callback) {
	var r = Ajax.prepare('HEAD', url, null, callback);
	if (r != null) {
		if (Ajax.request != null) {
			Ajax.queue.push(r);
			if (Ajax.timeout()) {
				Ajax.execute(Ajax.queue.shift());
			}
			return;
		}
		Ajax.execute(r);
	}
}
//---------------------------------------------------------------------------
Ajax.send = function(url, params, callback) {
	var r = Ajax.prepare('POST', url, params, callback, alert);
	if (r != null) {
		if (Ajax.request != null) {
			Ajax.queue.push(r);
			if (Ajax.timeout()) {
				Ajax.execute(Ajax.queue.shift());
			}
			return;
		}
		Ajax.execute(r);
	}
}
//---------------------------------------------------------------------------
Ajax.recv = function() {
	if (Ajax.request.readyState == 4) {
		if (Ajax.request.status == 200) {
			if (Ajax.request.callback != null) {
				Ajax.request.callback(Ajax.request.responseText);
			}
		} else if (Ajax.request.onerror) {
			Ajax.request.onerror('Request error:' + Ajax.request.statusText);
		}
		Ajax.execute(Ajax.queue.shift());
	}
}
//---------------------------------------------------------------------------
Ajax.refresh = function() {
	setTimeout('Ajax.refresh()', 5000);
	if (Ajax.request == null && Ajax.queue.length > 0)
		Ajax.execute(Ajax.queue.shift());
	else if (Ajax.request != null && Ajax.timeout() && Ajax.request.ontimeout != null)
		Ajax.request.ontimeout(Ajax.request);
}
//---------------------------------------------------------------------------
Ajax.time = 0;
Ajax.queue = [];
Ajax.request = null;
//---------------------------------------------------------------------------
Ajax.refresh();
//---------------------------------------------------------------------------
